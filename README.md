# Fabric.js 学习Demo

## 仓库简介

这个仓库存放了我学习 `fabric.js` 的历程。

其中包括：
- 常用 `api` 的使用方法；
- 功能实战 `demo`；

<br>

本仓库所有demo都使用原生方式开发。

**如果你想看看在 `Vue 3` 中如何使用 `fabric.js` ，可以看 [前端可视化笔记](https://gitee.com/k21vin/front-end-data-visualization) 这个仓库。**

<br><br>

## 一些阅读资料

> 如果你是从零起步学习 `fabric.js` ，强烈推荐你阅读 [ :closed_book: 《Fabric.js 从入门到________》](https://juejin.cn/post/7026941253845516324)。

<br>

| 图文教程 | 原生代码 | 在vue3中的用法 |
|---|---|---|
| [创建画布](https://juejin.cn/post/7026941253845516324#heading-13) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Canvas/newCanvas.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Basic/pages/Stated/Stated.vue) |
| [设置画布背景色](https://juejin.cn/post/7026941253845516324#heading-20) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Canvas/backgroundColor.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Basic/pages/Stated/Stated.vue) |
| [动态设置背景宽高](https://juejin.cn/post/7053049468601499684) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/demos/UploadImg/index.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Basic/pages/SetCanvasWH/SetCanvasWH.vue) |
| [将本地图像上传到画布背景](https://juejin.cn/post/7055201274693681160) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Canvas/setWH.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Demo/pages/UploadImg/UploadImg.vue) |
| [自定义右键菜单](https://juejin.cn/post/7051373700209180679) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/demos/ContextMenu/index.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Demo/pages/ContextMenu/ContextMenu.vue) |
| [更换图片(处理存在缓存的情况)](https://juejin.cn/post/7052719026874613773) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/demos/changeImage/index.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Demo/pages/ChangeImage/ChangeImage.vue) |
| [删除元素（带过渡动画）](https://juejin.cn/post/7056599707094614024) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Canvas/remove.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Basic/pages/RemoveObj/RemoveObj.vue) |
| 背景不受视口变换影响 | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Canvas/backgroundVpt.html) |  |
| 居中元素 | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Canvas/centerObject.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Basic/pages/CenterObject/CenterObject.vue) |
| 操作控件在裁剪区外显示 | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Canvas/controlsAboveOverlay.html) |  |
| 精简toObject | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Canvas/includeDefaultValues.html) |  |
| 元素选中时保持原来层级 | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Canvas/preserveObjectStacking.html) |  |
| 元素选中时保持原来层级（按着alt可继续选中） | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Canvas/altSelectionKey.html) |  |
| [摆正元素](https://juejin.cn/post/7057392358391808008) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Canvas/straightenObject.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Basic/pages/StraightenObject/StraightenObject.vue) |
| 平移画布 | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Canvas/absolutePan.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Basic/pages/AbsolutePan/AbsolutePan.vue) |
| 控制图层层级 | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Canvas/moveTo.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Basic/pages/MoveTo/MoveTo.vue) |
| [自由绘制矩形](https://juejin.cn/post/7058093223566114847) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/demos/FreeDrawing/createRect.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Demo/pages/CreateRect/CreateRect.vue) |
| 自由绘制圆形 | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/demos/FreeDrawing/createCircle.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Demo/pages/CreateCircle/CreateCircle.vue) |
| 自由绘制椭圆形 | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/demos/FreeDrawing/createEllipse.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Demo/pages/CreateEllipse/CreateEllipse.vue) |
| 自由绘制三角形 | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/demos/FreeDrawing/createTriangle.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Demo/pages/CreateTriangle/CreateTriangle.vue) |
| 自由绘制线段 | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/demos/FreeDrawing/createLine.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Demo/pages/CreateLine/CreateLine.vue) |
| 自由绘制折线 | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/demos/FreeDrawing/createPolyline.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Demo/pages/CreatePolyline/CreatePolyline.vue) |
| 自由绘制折线 | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/demos/FreeDrawing/createPolygon.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Demo/pages/CreatePolygon/CreatePolygon.vue) |

<br><br>

## 最后

最后，我非常希望你能在 **我的文章评论区** 里跟我 **斗图** :volcano:  :volcano:  :volcano:

别忘了点赞和收藏~